"""Setup configuration and dependencies for cf_bot."""

from setuptools import find_packages, setup

REQUIREMENTS = open("requirements.txt").readlines()

COMMANDS = [
    "mudpy=mudpy.app:main",
]

setup(
    name="mudpy",
    version="0.0.0.alpha0",
    author="",
    author_email="",
    url="",
    include_package_data=True,
    description="Basic MUD client implementation",
    packages=find_packages("src"),
    package_dir={"": "src",},
    python_requires=">=3.6.6",
    entry_points={"console_scripts": COMMANDS},
    install_requires=REQUIREMENTS,
)
