"""Module for configuring mudpy."""

import json
import logging
import os

LOGGER = logging.getLogger(__name__)

_CONFIG_FILENAME = "config.json"
_BASE_CONFIG_PATH_FOLDER = ".mudpy"
_BASE_PATH = os.path.expanduser("~")

MUDPY_PATH = os.path.join(_BASE_PATH, _BASE_CONFIG_PATH_FOLDER)
CONFIG_PATH = os.path.join(MUDPY_PATH, _CONFIG_FILENAME)

DEFAULT_CONFIG = {
    "gui": {"title": "MudPy", "geometry": "1000x800", "start_zoomed": True,},
    "colormap": {
        "background": "black",
        "foreground": "lime green",
        "terminal": {
            "0": "lime green",
            "1": "snow",
            "31": "orange red",
            "32": "green",
            "33": "yellow",
            "35": "magenta2",
            "36": "cyan",
            "37": "white",
        },
    },
}

# REQUIRED CONFIG SETTINGS
RECEIVE_DELAY = 0.05


def save_config(config_dict: dict) -> None:
    """Save configuration to file."""
    with open(CONFIG_PATH, "w") as config_file:
        json.dump(config_dict, config_file, indent=4)
    LOGGER.info("Saved config.")


def load_config() -> dict:
    """Save configuration to from file."""
    with open(CONFIG_PATH) as config_file:
        config: dict = json.load(config_file)
    LOGGER.info("Loaded config.")
    return config


def validate_config() -> dict:
    """Validate the configuration and load it."""
    if not os.path.exists(MUDPY_PATH):
        os.makedirs(MUDPY_PATH)
        LOGGER.info("No config path detected - created %s", MUDPY_PATH)

    if not os.path.exists(CONFIG_PATH):
        LOGGER.info("No config file detected - created default config  %s", CONFIG_PATH)
        save_config(DEFAULT_CONFIG)

    config = load_config()
    return config


CONFIG = validate_config()
