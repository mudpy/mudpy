"""Functions to set and control colors in mudpy."""

import logging
from tkinter import END, Event
from typing import Any, Callable
from mudpy.config.colors import add_custom_tags
from . import CONFIG

LOGGER = logging.getLogger(__name__)


def configure_mudpy_gui(mudpy_instance: Any) -> None:
    """Configure the gui of a mudpy instance."""
    _set_main_attrs(mudpy_instance)
    _set_colors(mudpy_instance)
    _make_text_readonly(mudpy_instance)
    _make_gui_sticky(mudpy_instance)
    _focus_entry_field(mudpy_instance)
    _bind_history(mudpy_instance)


def _set_colors(mudpy_instance: Any) -> None:
    """Set colors of the GUI."""
    cconf = CONFIG["colormap"]
    foreground = cconf["foreground"]
    background = cconf["background"]
    terminal = cconf["terminal"]
    mudpy_instance.text_box.configure(foreground=foreground, background=background)
    for color_key, color_string in terminal.items():
        mudpy_instance.text_box.tag_config(color_key, foreground=color_string, background=background)
    add_custom_tags(mudpy_instance)
    LOGGER.info("Colors loaded.")


def _set_main_attrs(mudpy_instance: Any) -> None:
    """Set main attributes of GUI."""
    gui_conf = CONFIG["gui"]
    mudpy_instance.app.title(gui_conf.get("title") or "MudPy")
    mudpy_instance.app.geometry(gui_conf.get("geometry") or "1000x800")
    if gui_conf.get("start_zoomed"):
        mudpy_instance.app.wm_state("zoomed")
    mudpy_instance.app.columnconfigure(0, weight=1)
    mudpy_instance.app.rowconfigure(0, weight=1)
    LOGGER.info("Main Attributes set.")


def _make_text_readonly(mudpy_instance: Any) -> None:
    """Make text box readonly."""
    # Make it so the text box isn't editable.
    mudpy_instance.text_box.bind("<Key>", lambda e: "break")
    mudpy_instance.text_box.bind("<Button-1>", lambda e: "break")
    mudpy_instance.text_box.bind("<B1-Motion>", lambda e: "break")
    mudpy_instance.text_box.bind("<ButtonRelease-1>", lambda e: "break")
    mudpy_instance.entry_field.bind("<Return>", mudpy_instance.send)
    LOGGER.info("Text box set to read only.")


def _make_gui_sticky(mudpy_instance: Any) -> None:
    """Make GUI sticky."""
    # Make the GUI sticky and expandable
    mudpy_instance.text_box.grid(row=0, column=0, sticky="nsew")
    mudpy_instance.scrollbar.grid(row=0, column=1, sticky="nse")
    mudpy_instance.entry_field.grid(row=1, sticky="sew")
    LOGGER.info("GUI is now sticky icky.")


def _focus_entry_field(mudpy_instance: Any) -> None:
    """Focus entry field."""
    # When you start up, bring focus to the entry panel and
    # select all so it overrides.
    mudpy_instance.entry_field.focus()
    mudpy_instance.entry_field.select_range(0, END)
    LOGGER.info("Cursor focused on entry field.")


class RootCallback:
    """Class for commands that need to callback with access to root object."""

    # pylint: disable=too-few-public-methods

    def __init__(self, mudpy_instance: Any, func: Callable):
        """Initialize a root callback class instance."""
        self.mudpy_instance = mudpy_instance
        self.func = func

    def __call__(self, event: Event) -> Any:
        """Run the function against the mudpy instance."""
        self.func(self.mudpy_instance, event)


def _prev_hist(mudpy_instance: Any, event: Event) -> None:  # pylint: disable=unused-argument
    """Change entry to previous history item."""
    LOGGER.debug("Called previous history command")
    if not mudpy_instance.history:
        LOGGER.debug("No history yet.")
        return
    mudpy_instance.history_index -= 1
    if mudpy_instance.history_index < 0:
        mudpy_instance.history_index = 0
    hist_command = mudpy_instance.history[mudpy_instance.history_index]
    LOGGER.debug("Seting history to index %s: %s", mudpy_instance.history_index, hist_command)
    mudpy_instance.entry_field.delete(0, END)
    mudpy_instance.entry_field.insert(0, hist_command)
    mudpy_instance.entry_field.select_range(0, END)


def _next_hist(mudpy_instance: Any, event: Event) -> None:  # pylint: disable=unused-argument
    """Change entry to next history item."""
    LOGGER.debug("Called next history command")
    if not mudpy_instance.history:
        LOGGER.debug("No history yet.")
        return
    mudpy_instance.history_index += 1
    max_hist_len = len(mudpy_instance.history) - 1
    if mudpy_instance.history_index > max_hist_len:
        mudpy_instance.history_index = max_hist_len
    hist_command = mudpy_instance.history[mudpy_instance.history_index]
    LOGGER.debug("Seting history to index %s: %s", mudpy_instance.history_index, hist_command)
    mudpy_instance.entry_field.delete(0, END)
    mudpy_instance.entry_field.insert(0, hist_command)
    mudpy_instance.entry_field.select_range(0, END)


def _bind_history(mudpy_instance: Any) -> None:
    """Bind the history behavior."""
    prev_hist_callback = RootCallback(mudpy_instance, _prev_hist)
    next_hist_callback = RootCallback(mudpy_instance, _next_hist)
    mudpy_instance.entry_field.bind("<Up>", prev_hist_callback)
    mudpy_instance.entry_field.bind("<Down>", next_hist_callback)
