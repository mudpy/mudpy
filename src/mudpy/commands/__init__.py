"""Module for handling special commands."""
import logging
from typing import Any

from .system import _disconnect
from .system import _quit_close
from .system import _reconnect


LOGGER = logging.getLogger(__name__)


COMMANDS = {"qc": _quit_close, "disconnect": _disconnect, "reconnect": _reconnect}


def handle_command(mudpy_instance: Any, command: str) -> bool:
    """Handle a system command."""
    executed = False
    if command in COMMANDS:
        cmd_run = COMMANDS[command]
        try:
            LOGGER.info("Handling system command %s", command)
            cmd_run(mudpy_instance, command)
            LOGGER.info("Finished handling system command %s", command)
            executed = True
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception("Error running command %s", command)
    return executed
