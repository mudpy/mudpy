#!/usr/bin/env python3
"""Script for Tkinter GUI telnet client."""

import argparse
import logging
from mudpy.client import MudPyClient
from mudpy.config.gui import configure_mudpy_gui

LOGGER = logging.getLogger("mudpy")


def main() -> None:
    """Run main logic."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--controller", "-c", action="store_true", help="Open a socket for a controller to connect to.")
    args = parser.parse_args()
    LOGGER.info("Spinning up client.")
    client = MudPyClient(add_controller=args.controller)
    configure_mudpy_gui(client)
    try:
        client.app.mainloop()
    except SystemExit:
        client.quit()


if __name__ == "__main__":
    main()
