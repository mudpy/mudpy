"""Basic testing of importing the module."""


def test_import() -> None:
    """Test that we can import the module."""
    # This makes sure it installs properly.
    import mudpy  # pylint: disable=unused-import,import-outside-toplevel
